//
//  AvatarPickerVC.swift
//  ChatApp
//
//  Created by hossein shademany on 11/13/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class AvatarPickerVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    // Variables
    var avatarType = AavatarType.dark

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "AvatarCell", bundle: nil), forCellWithReuseIdentifier: "avatarCellId")
    
    }
    // cell count
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 27
    }
    //cell content
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "avatarCellId", for: indexPath) as? AvatarCell {
            cell.AvatarColor(index: indexPath.item, type: avatarType)
            return cell
        }
        return AvatarCell()
    }
    // dynamic cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var numberOfColums : CGFloat = 3
        if UIScreen.main.bounds.width > 320 {
            numberOfColums = 4
        }
        let spaceBetweenCells : CGFloat = 10
        let padding : CGFloat = 40
        let cellDimention = ((collectionView.bounds.width - padding) - (numberOfColums - 1) * spaceBetweenCells) / numberOfColums
        return CGSize(width: cellDimention, height: cellDimention)
    }

    // selecting cells
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if avatarType == .dark {
            UserDataServices.instance.updataAvatarName(avatarName: "dark\(indexPath.item)")
        }else {
             UserDataServices.instance.updataAvatarName(avatarName: "light\(indexPath.item)")
        }
        self.dismiss(animated: true, completion: nil)
    }
    //segment control
    @IBAction func segmentBtn(_ sender: Any) {
        if segmentedControl.selectedSegmentIndex == 0 {
            avatarType = .dark
        }else{
            avatarType = .light
        }
        collectionView.reloadData()
    }
    //dismiss
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
