//
//  ChannelVC.swift
//  ChatApp
//
//  Created by hossein shademany on 11/8/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userImage: CircleImage!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        SocketServices.instance.getChannel { (success) in
            if success{
                self.tableView.reloadData()
            }
        }
//        if AuthServices.instance.loggedIn{
//            MessageServices.instance.getChannels { (success) in
//                self.tableView.reloadData()
//            }
//        }
       
    }
    // view did apear
    override func viewDidAppear(_ animated: Bool) {
        userInfo()
    }
    // set up the views
    func setupView(){
        // tableView setup
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib.init(nibName: "ChannelCell", bundle: nil), forCellReuseIdentifier: "channelCellID")
        // swreveal setup
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
        // Notifications
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NOTIF_USER_CHANEGED, object: nil)

    }
    // reload data for channels
    @objc func reloadData(){
        userInfo()
        if AuthServices.instance.loggedIn{
            MessageServices.instance.getChannels { (success) in
                if success{
                    self.tableView.reloadData()
                }
            }
        }
    }
    // add channel button pressed
    @IBAction func addChannelBtn(_ sender: Any) {
        if AuthServices.instance.loggedIn{
            let addChannel = AddChannelVC()
            addChannel.modalPresentationStyle = .custom
            present(addChannel, animated: true, completion: nil)
        }else{
            let alertController = UIAlertController(title: "Login first!", message: nil, preferredStyle: .alert)
            let addButton = UIAlertAction(title: "ok!", style: .cancel, handler: nil)
            alertController.addAction(addButton)
            present(alertController, animated: true, completion: nil)
        }
        
    }
    // login button pressed
    @IBAction func toLogin(_ sender: Any) {
        if AuthServices.instance.loggedIn {
            let profile = ProfileVC()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true, completion: nil)

        }else{
            performSegue(withIdentifier: TO_LOGIN, sender: nil)
        }
    }
    // user infos
    func userInfo(){
        if AuthServices.instance.loggedIn {
            userImage.image = UIImage(named: UserDataServices.instance.avatarName)
            loginBtn.setTitle(UserDataServices.instance.name, for: .normal)
            userImage.backgroundColor = UserDataServices.instance.userColor(component: UserDataServices.instance.avatarColor)
        }else{
            userImage.image = UIImage(named: "profileDefault")
            loginBtn.setTitle("log in", for: .normal)
            userImage.backgroundColor = UIColor.clear
            tableView.reloadData()
        }
    }
    // tableView content
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "channelCellID", for: indexPath) as? ChannelCell {
            let channel  = MessageServices.instance.channels[indexPath.row]
            cell.config(channel: channel)
            return cell
        }else{
            return ChannelCell()
        }
    }
    // count of cells
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageServices.instance.channels.count
    }
    // selected item
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = MessageServices.instance.channels[indexPath.row]
        MessageServices.instance.selectedChannel = channel
        NotificationCenter.default.post(name: NOTIF_CHANNEL_SELECTED, object: nil)
        revealViewController().revealToggle(true)
    }
    
    
}
