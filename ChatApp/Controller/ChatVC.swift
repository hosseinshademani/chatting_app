//
//  ChatVC.swift
//  ChatApp
//
//  Created by hossein shademany on 11/8/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class ChatVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    // Outlets
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var typingUsers: UILabel!
    @IBOutlet weak var chatTitle: UILabel!
    @IBOutlet weak var burgerBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    // segues
    @IBAction func unwindSegue(segue : UIStoryboardSegue){}
    
    // variables
    var istyping = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        if AuthServices.instance.loggedIn{
            AuthServices.instance.findUserByID(completion: { (success) in
                if success{
                    
                }
            })
        }
        // dismiss keyboard
        let tapOnAlertView = UITapGestureRecognizer(target: self, action: #selector(keyboradDismiss))
        view.addGestureRecognizer(tapOnAlertView)
        sendBtn.isHidden = true
        
    }
    // set up view
    func setupView(){
        view.bindToKeyboard()
        onLoginChannel()
        // tableView setup
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: "messageCell")
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        // socket
        SocketServices.instance.getMessages{ (success) in
            if success{
                self.tableView.reloadData()
                let endIndex = IndexPath(row: MessageServices.instance.messages.count - 1, section: 0)
                self.tableView.scrollToRow(at: endIndex, at: .bottom, animated: true)
            }
        }
        // typing users
        SocketServices.instance.getTypingUsers { (typers) in
            guard let channelId = MessageServices.instance.selectedChannel?.id else {return}
            var name = ""
            var typersCount = 0
            for (typingUser,channel) in typers{
                if typingUser != UserDataServices.instance.name && channel == channelId{
                    if name == "" {
                        name = typingUser
                    }else{
                        name = "\(name) & \(typingUser)"
                        
                    }
                    typersCount += 1
                }
            }
            if AuthServices.instance.loggedIn && typersCount > 0 {
                var verb  = "is"
                if typersCount > 1 {
                    verb = "are"
                }
                self.typingUsers.text = "\(name) \(verb) typing a message"
            }else{
                self.typingUsers.text = ""
            }
            
        }
        // touch settings
        burgerBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        // notifications
        NotificationCenter.default.addObserver(self, selector: #selector(chatTitleUpdate), name: NOTIF_CHANNEL_SELECTED, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NOTIF_USER_CHANEGED, object: nil)
    }
    //
    @objc func reloadData(){
        if AuthServices.instance.loggedIn{
            
        }else{
            tableView.reloadData()
        }
    }
    //
    @objc func keyboradDismiss(){
        view.endEditing(true)
    }
    // send message button
    @IBAction func sendBtn(_ sender: Any) {
        if AuthServices.instance.loggedIn && messageTextField.text != ""{
            guard let channelId = MessageServices.instance.selectedChannel?.id else {return}
            guard let messageBody = messageTextField.text else{return}
            SocketServices.instance.sendMessage(messageBody: messageBody, userId: UserDataServices.instance.id, channelId: channelId, completion: { (success) in
                self.messageTextField.text = ""
                self.messageTextField.resignFirstResponder()
                  SocketServices.instance.socket.emit("stopType", UserDataServices.instance.name,channelId)
                self.sendBtn.isHidden = true
            })
            
        }
    }
    // text box changing
    @IBAction func textBox(_ sender: Any) {
        guard let channelId = MessageServices.instance.selectedChannel?.id else{return}
        if messageTextField.text == "" {
            self.istyping = false
            sendBtn.isHidden = true
            SocketServices.instance.socket.emit("stopType", UserDataServices.instance.name,channelId)
        }else{
            sendBtn.isHidden = false
            SocketServices.instance.socket.emit("startType", UserDataServices.instance.name,channelId)
            self.istyping = true
        }
    }
    //-----
    @objc func chatTitleUpdate(){
        let channelName = MessageServices.instance.selectedChannel?.channelTitle ?? ""
        chatTitle.text = "#\(channelName)"
        self.getChannelsId()
    }
    // on the first log in
    func onLoginChannel(){
        if AuthServices.instance.loggedIn{
            MessageServices.instance.getChannels { (success) in
                if success{
                    if MessageServices.instance.channels.count > 0 {
                        MessageServices.instance.selectedChannel = MessageServices.instance.channels[0]
                        self.chatTitleUpdate()
                    }else{
                        self.chatTitle.text = "No channel selected1"
                    }
                }
            }
        }else{
            tableView.reloadData()
        }
    }
    // get messages
    func getChannelsId(){
        guard let channelId = MessageServices.instance.selectedChannel?.id else {return}
        MessageServices.instance.getMessagesForChannels(channelId: channelId) { (success) in
            if success{
                self.tableView.reloadData()
                //                scroll to end
                if MessageServices.instance.messages.count > 1 {
                    let endIndex = IndexPath(row: MessageServices.instance.messages.count - 1, section: 0)
                    self.tableView.scrollToRow(at: endIndex, at: .bottom, animated: true)
                }
               
            }
        }
    }
    // cell content
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell") as? MessageCell{
            let message = MessageServices.instance.messages[indexPath.row]
            cell.config(message: message)
            return cell
        }else{
            return MessageCell()
        }
    }
    // cell count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageServices.instance.messages.count
    }
    
}
