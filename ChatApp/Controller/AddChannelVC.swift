//
//  AddChannelVC.swift
//  ChatApp
//
//  Created by hossein shademany on 11/20/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class AddChannelVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var channelName: UITextField!
    @IBOutlet weak var channelDescrtiption: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    // view setups
    func setupView(){
       
        channelName.attributedPlaceholder = NSAttributedString(string: "name", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.3130097389, green: 0.5287408233, blue: 0.8230261207, alpha: 0.5188722201)])
        channelDescrtiption.attributedPlaceholder = NSAttributedString(string: "description", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.3130097389, green: 0.5287408233, blue: 0.8230261207, alpha: 0.5188722201)])
        
        
        let tapOnMainView = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        mainView.addGestureRecognizer(tapOnMainView)
        let tapOnAlertView = UITapGestureRecognizer(target: self, action: #selector(keyboradDismiss))
        alertView.addGestureRecognizer(tapOnAlertView)
    }
    @objc func dismissView(){
        dismiss(animated: true, completion: nil)
    }
    @objc func keyboradDismiss(){
        alertView.endEditing(true)
    }
    // close button pressed
    @IBAction func dismissBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // creat channel button pressed
    @IBAction func creatChannelPressed(_ sender: Any) {
        guard let channelName = self.channelName.text , self.channelName.text != "" else{return}
        guard let channelDescription = self.channelDescrtiption.text , self.channelDescrtiption.text != "" else{return}
        SocketServices.instance.addChannel(channelName: channelName, channelDescription: channelDescription) { (success) in
            if success{
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    
    
    
    
}
