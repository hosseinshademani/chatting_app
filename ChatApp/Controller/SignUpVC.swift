//
//  SignUpVC.swift
//  ChatApp
//
//  Created by hossein shademany on 11/9/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
    
    //outlets
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var userImage: UIImageView!
    
    //variables
    var avatarName = "profileDefault"
    var avatarColor = "[0.5, 0.5, 0.5, 1]"
    var bgColor : UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDataServices.instance.avatarName != "" {
            userImage.image = UIImage(named: UserDataServices.instance.avatarName)
            avatarName = UserDataServices.instance.avatarName
            if avatarName.contains("light") && bgColor == nil {
                userImage.backgroundColor = UIColor.lightGray
            }
        }
    }
    @IBAction func dismissBtn(_ sender: Any) {
        performSegue(withIdentifier: TO_UNWIND, sender: nil)
    }
    @IBAction func creatAccountBtn(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        //user info.
        guard let name = userName.text , userName.text != "" else{return}
        guard let email = userEmail.text , userEmail.text != "" else{return}
        guard let pass = password.text , password.text != "" else{return}
        //creat a user
        AuthServices.instance.AuthUser(email: email, password: pass) { (success) in
            if success {
                print("user generated")
                // login user
                AuthServices.instance.loginUser(email: email, password: pass, completion: { (success) in
                    if success {
                        print("user logged in")
                        // add the user
                        AuthServices.instance.addUser(name: name, email: email, avatarName: self.avatarName, avatarColor: self.avatarColor, completion: { (success) in
                            if success{
                                
                                print("user added")
                                self.spinner.isHidden = true
                                self.spinner.stopAnimating()
                                self.performSegue(withIdentifier: TO_UNWIND, sender: nil)
                                NotificationCenter.default.post(name: NOTIF_USER_CHANEGED, object: nil)
                            }
                        })
                    }
                })
            }
        }
        
    }
    // chose avatar
    @IBAction func choseAvatarBtn(_ sender: Any) {
        performSegue(withIdentifier: TO_AVATAR_PICKER, sender: nil)
    }
    // generate background
    @IBAction func choseBackgroundBtn(_ sender: Any) {
        let r = CGFloat(arc4random_uniform(255)) / 255
        let g = CGFloat(arc4random_uniform(255)) / 255
        let b = CGFloat(arc4random_uniform(255)) / 255
        UIView.animate(withDuration: 0.2) {
            self.bgColor = UIColor(red: r, green: g, blue: b, alpha: 1)
            self.userImage.backgroundColor = self.bgColor
            self.avatarColor = "[\(r), \(g), \(b), 1]"
        }
    }
    // View attributes
    func setupView(){
        spinner.isHidden = true
        userName.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.3130097389, green: 0.5287408233, blue: 0.8230261207, alpha: 0.5188722201)])
        password.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.3130097389, green: 0.5287408233, blue: 0.8230261207, alpha: 0.5188722201)])
        userEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.3130097389, green: 0.5287408233, blue: 0.8230261207, alpha: 0.5188722201)])
        //dismiss by touch
        let tap = UITapGestureRecognizer(target: self, action: #selector(keyboradDismiss))
        view.addGestureRecognizer(tap)
    }
    @objc func keyboradDismiss(){
        view.endEditing(true)
    }
}








