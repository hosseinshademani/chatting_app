//
//  LogInVC.swift
//  ChatApp
//
//  Created by hossein shademany on 11/9/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    // view setup
    func setupView(){
        spinner.isHidden = true
        userEmail.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.3130097389, green: 0.5287408233, blue: 0.8230261207, alpha: 0.5188722201)])
        userPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.3130097389, green: 0.5287408233, blue: 0.8230261207, alpha: 0.5188722201)])
        let tap = UITapGestureRecognizer(target: self, action: #selector(keyboradDismiss))
        view.addGestureRecognizer(tap)
    }
    // tap to dismiss keyboard
    @objc func keyboradDismiss(){
        view.endEditing(true)
    }
    // login pressd
    @IBAction func loginBtnPressed(_ sender: Any) {
        spinner.isHidden = false
        spinner.startAnimating()
        guard let email = userEmail.text , userEmail.text != "" else{return}
        guard let pass = userPassword.text , userPassword.text != "" else{return}
        // login the user
        AuthServices.instance.loginUser(email: email, password: pass) { (success) in
            if success{
                // find user info
                AuthServices.instance.findUserByID(completion: { (success) in
                    if success{
                        NotificationCenter.default.post(name: NOTIF_USER_CHANEGED, object: nil)
                        self.spinner.isHidden = true
                        self.spinner.stopAnimating()
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }else{
                self.spinner.isHidden = true
               self.spinner.stopAnimating()
            }
        }
    }
    // dismiss button pressed
    @IBAction func dismissBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    // sign up button
    @IBAction func signUpBtn(_ sender: Any) {
        performSegue(withIdentifier: TO_SIGN_UP, sender: nil)
    }
}
