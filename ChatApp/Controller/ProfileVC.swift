//
//  ProfileVCViewController.swift
//  ChatApp
//
//  Created by hossein shademany on 11/17/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    
    //configiration
    func config(){
        
        userImage.image = UIImage(named: UserDataServices.instance.avatarName)
        userImage.backgroundColor = UserDataServices.instance.userColor(component: UserDataServices.instance.avatarColor)
        userName.text = UserDataServices.instance.name
        userEmail.text = UserDataServices.instance.email
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.close))
        view.addGestureRecognizer(tap)
    }
    //close by tapping
    @objc func close(){
        dismiss(animated: true, completion: nil)
    }
    // logout
    @IBAction func logoutBtnPressed(_ sender: Any) {
        UserDataServices.instance.logout()
        
        self.dismiss(animated: true, completion: nil)
        
    }
    //dismiss
    @IBAction func dismissBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
