//
//  Message.swift
//  ChatApp
//
//  Created by hossein shademany on 11/22/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import Foundation
struct Message{
    
    public private(set) var id : String!
    public private(set) var messageBody : String!
    public private(set) var channelId : String!
    public private(set) var userName : String!
    public private(set) var userAvatar : String!
    public private(set) var userAvatarColor : String!
    public private(set) var timeStamp : String!
    
}
