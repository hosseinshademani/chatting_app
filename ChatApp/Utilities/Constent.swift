//
//  Constent.swift
//  ChatApp
//
//  Created by hossein shademany on 11/9/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import Foundation
typealias completionHandler = (_ success : Bool) -> ()
//URLs
let BASE_URL = "https://soorenchat.herokuapp.com/v1/"
let URL_REGISTERE  = "\(BASE_URL)account/register"
let URL_LOGIN = "\(BASE_URL)account/login"
let URL_ADD = "\(BASE_URL)user/add"
let URL_USER_BY_ID = "\(BASE_URL)user/byemail/"
let URL_CHANNEL = "\(BASE_URL)channel"
let URL_CHANNEL_MESSAGES = "\(BASE_URL)message/byChannel/"
 //Headers
let HEADER = [
    "Content-Type" : "application/json; charset=utf-8"
]
let BEARER_HEADER = [
    "Authorization" : "Bearer \(AuthServices.instance.authToken)",
    "Content-Type" : "application/json; charset=utf-8"
]
// Notifications
let NOTIF_USER_CHANEGED = Notification.Name("userDatachanged")
let NOTIF_CHANNEL_SELECTED = Notification.Name("channelSelected")
let NOTIF_CHANNEL_LOADED = Notification.Name("channelLoaded")

//segue
let TO_LOGIN = "toLogin"
let TO_SIGN_UP = "tosignUp"
let TO_UNWIND = "unwindtoChatVC"
let TO_AVATAR_PICKER = "toAvatarPicker"

// User defaults
let TOKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL = "userEmail"

