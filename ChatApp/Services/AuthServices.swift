//
//  AuthServices.swift
//  ChatApp
//
//  Created by hossein shademany on 11/10/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthServices{
    static let instance = AuthServices()
    
    // user defaults
    let defaults = UserDefaults.standard
    
    var loggedIn : Bool {
        get {
            return defaults.bool(forKey: LOGGED_IN_KEY)
        }
        set {
            defaults.set(newValue, forKey: LOGGED_IN_KEY)
        }
    }
    var authToken : String {
        get {
            return defaults.value(forKey: TOKEN_KEY) as! String
        }
        set {
            defaults.set(newValue, forKey: TOKEN_KEY)
        }
    }
    var userEmail : String {
        get {
            return defaults.value(forKey: USER_EMAIL) as! String
        }
        set {
            defaults.set(newValue, forKey: USER_EMAIL)
        }
    }
    
    //authorize the user
    func AuthUser(email : String , password : String ,completion : @escaping completionHandler ){
        let lowercaseEmail = email.lowercased()
        let body : [String : Any] = [
            "email"  : lowercaseEmail,
            "password" : password
        ]
        Alamofire.request(URL_REGISTERE, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            }else{
                completion(false)
                debugPrint("Athorization issue :/n\(response.result.error as Any)")
            }
        }
    }
    
    //login the user
    func loginUser(email : String , password : String , completion : @escaping completionHandler){
        let lowercaseEmail = email.lowercased()
        let body : [String : Any] = [
            "email"  : lowercaseEmail,
            "password" : password
        ]
        Alamofire.request(URL_LOGIN, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                let json = JSON(data: data)
                self.userEmail = json["user"].stringValue
                self.authToken = json["token"].stringValue
                self.loggedIn = true
                completion(true)
            }else{
                completion(false)
                debugPrint("login issue:/n\(response.result.error as Any)")
                debugPrint()
            }
        }
    }
    
    // add a user
    func addUser(name : String , email : String , avatarName : String , avatarColor : String , completion : @escaping completionHandler){
        let lowerCaseEmail = email.lowercased()
        let body : [String : Any] = [
            "avatarName" : avatarName,
            "avatarColor" : avatarColor,
            "name" : name,
            "email" : lowerCaseEmail
        ]
        Alamofire.request(URL_ADD, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                self.getUSerInfo(data: data)
                completion(true)
            }else {
                completion(false)
                debugPrint("status: \(response.response?.statusCode as Any)")
                debugPrint("add user issue: \(response.result.error as Any)")
            }
        }
    }
    // find user by id
    func findUserByID(completion : @escaping completionHandler){
        Alamofire.request("\(URL_USER_BY_ID)\(userEmail)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                self.getUSerInfo(data: data)
                completion(true)
            }else {
                completion(false)
                debugPrint("status: \(response.response?.statusCode as Any)")
                debugPrint("issue with finding by id: \(response.result.error as Any)")
            }
        }
    }
    // get user info
    func getUSerInfo(data : Data){
         let json = JSON(data : data)
        let id = json["_id"].stringValue
        let name = json["name"].stringValue
        let email = json["email"].stringValue
        let avatarName = json["avatarName"].stringValue
        let avatarColor = json["avatarColor"].stringValue
        
           UserDataServices.instance.setUserData(name: name, avatarName: avatarName, avatarColor: avatarColor, email: email, id: id)
    }
    
    
    
    
    
    
    
    
    
    
    
}
