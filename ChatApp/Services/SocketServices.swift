//
//  SocketServices.swift
//  ChatApp
//
//  Created by hossein shademany on 11/20/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit
import SocketIO
class SocketServices: NSObject {
    
    static let instance = SocketServices()
    
    // variables
    var socket : SocketIOClient = SocketIOClient(socketURL: URL(string: BASE_URL)!)
    
    override init() {
        super.init()
    }
  
    
    // add a channel
    func addChannel(channelName: String , channelDescription : String , completion : @escaping completionHandler){
        socket.emit("newChannel", channelName,channelDescription)
        completion(true)
      
    }

    // get all channels
    func getChannel(completion: @escaping completionHandler) {
        socket.on("channelCreated") { (dataArray, ack) in
            guard let channelName = dataArray[0] as? String else { return }
            guard let channelDesc = dataArray[1] as? String else { return }
            guard let channelId = dataArray[2] as? String else { return }
            
            let newChannel = Channel(channelTitle: channelName, channelDescription: channelDesc, id: channelId)
            MessageServices.instance.channels.append(newChannel)
            completion(true)
        }
    }
    // send messages
    func sendMessage(messageBody: String ,userId: String , channelId: String , completion : @escaping completionHandler){
        let user = UserDataServices.instance
        socket.emit("newMessage", messageBody, userId, channelId, user.name, user.avatarName, user.avatarColor)
        completion(true)
    }
    func getMessages(completion : @escaping completionHandler){
        socket.on("messageCreated") { (dataAray, ack) in
            guard let msgbody = dataAray[0] as? String else{  return }
             guard let channelId = dataAray[2] as? String else{  return }
             guard let userName = dataAray[3] as? String else{  return }
             guard let userAvatar = dataAray[4] as? String else{  return }
             guard let userAvatarColor = dataAray[5] as? String else{  return }
             guard let id = dataAray[6] as? String else{  return }
            guard let timeStamp = dataAray[7] as? String else{  return }
           
            if channelId == MessageServices.instance.selectedChannel?.id && AuthServices.instance.loggedIn {
                let newMessage = Message(id: id, messageBody: msgbody, channelId: channelId, userName: userName, userAvatar: userAvatar, userAvatarColor: userAvatarColor, timeStamp: timeStamp)
                MessageServices.instance.messages.append(newMessage)
                completion(true)
            }else{
                completion(false)
            }
        }
    }
    //typing users
    func getTypingUsers(_ completionHandler : @escaping (_ typers: [String : String])-> Void){
        socket.on("userTypingUpdate") { (dataAry, ack) in
            guard let typingUsers = dataAry[0] as? [String: String] else{return}
            completionHandler(typingUsers)
        }
    }
//    disconnect the socket
    func connect(){
        socket.connect()
    }
    // disconnect the socket
    func disconnect(){
        socket.disconnect()
    }
   
}
