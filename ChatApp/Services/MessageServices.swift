//
//  MessageServices.swift
//  ChatApp
//
//  Created by hossein shademany on 11/18/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class MessageServices {
    static let instance = MessageServices()
    
    // Variables
    var channels = [Channel]()
    var messages = [Message]()
    var selectedChannel : Channel?
  
    // get the channels
    func getChannels(completion : @escaping completionHandler){
        
        Alamofire.request(URL_CHANNEL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            
            if response.result.error == nil {
                guard let data = response.data else{return}
                if let json = JSON(data: data).array {
                    for item in json {
                        let title = item["name"].stringValue
                        let description = item["descrtiption"].stringValue
                        let id = item["_id"].stringValue
                        let channel = Channel(channelTitle: title, channelDescription: description, id: id)
                        self.channels.append(channel)
                    }
                }
                print("channels\n:\(self.channels)")
                completion(true)
            }else{
                completion(false)
                debugPrint("getting channel error:\n" ,response.result.error as Any)
            }
        }
    }
    
    //get the messages
    func getMessagesForChannels(channelId : String, completion : @escaping completionHandler){
        clearMessages()
        Alamofire.request("\(URL_CHANNEL_MESSAGES)\(channelId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil{
                guard let data = response.data else{return}
                if let json  = JSON(data: data).array{
                    for item in json{
                        let userName = item["userName"].stringValue
                        let userAvatar = item["userAvatar"].stringValue
                        let userAvatarColor = item["userAvatarColor"].stringValue
                        let timeStamp = item["timeStamp"].stringValue
                        let id = item["_id"].stringValue
                        let messageBody = item["messageBody"].stringValue
                        let channelId = item["channelId"].stringValue
                       let message = Message(id: id, messageBody: messageBody, channelId: channelId, userName: userName, userAvatar: userAvatar, userAvatarColor: userAvatarColor, timeStamp: timeStamp)
                        self.messages.append(message)
                    }
                }
                
                print("messages------",self.messages)
                completion(true)
            }else{
                debugPrint("getting error for message:/n" ,response.result.error as Any)
                completion(false)
            }
         
        }
    }
    
    // clear messages
    func clearMessages(){
        messages.removeAll()
    }
    // clear the channel
    func clearChannel(){
        channels.removeAll()
    }
}








