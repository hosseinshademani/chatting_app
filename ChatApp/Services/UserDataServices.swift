//
//  UserDataServices.swift
//  ChatApp
//
//  Created by hossein shademany on 11/12/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import Foundation
class UserDataServices {
    static let instance = UserDataServices()
    // Properties
    public private(set) var id : String = ""
    public private(set) var name : String = ""
    public private(set) var avatarColor : String = ""
    public private(set) var avatarName : String = ""
    public private(set) var email : String = ""
    
    // set the user data
    func setUserData(name : String , avatarName : String , avatarColor : String , email : String , id : String){
        self.id = id
        self.name = name
        self.email = email
        self.avatarColor = avatarColor
        self.avatarName = avatarName
    }
    
    // get the avatar name
    func updataAvatarName(avatarName : String){
        self.avatarName = avatarName
    }
    
    // get the user avatar color
    func userColor(component : String)->UIColor{
// "[0.925490196078431, 0.631372549019608, 0.996078431372549, 1]"
        
        let scanner = Scanner(string: component)
        
        let skip = CharacterSet(charactersIn: "[], ")
        let comma = CharacterSet(charactersIn: ",")
        scanner.charactersToBeSkipped = skip
        
        var r, b, g, a : NSString?
        scanner.scanUpToCharacters(from: comma, into: &r)
        scanner.scanUpToCharacters(from: comma, into: &g)
        scanner.scanUpToCharacters(from: comma, into: &b)
        scanner.scanUpToCharacters(from: comma, into: &a)
       
        let defaultColor  = UIColor.lightGray
        
        guard let rUnwraped = r else{return defaultColor}
        guard let gUnwraped = g else{return defaultColor}
        guard let bUnwraped = b else{return defaultColor}
        guard let aUnwraped = a else{return defaultColor}
        
           let rFloat = CGFloat(rUnwraped.doubleValue)
           let gFloat = CGFloat(gUnwraped.doubleValue)
           let bFloat = CGFloat(bUnwraped.doubleValue)
           let aFloat = CGFloat(aUnwraped.doubleValue)
        
        let newColor = UIColor(red: rFloat, green: gFloat, blue: bFloat, alpha: aFloat)
        return newColor
    }
    
    //logout
    func logout(){
        self.id = ""
        self.name = ""
        self.email = ""
        self.avatarColor = ""
        self.avatarName = ""
        AuthServices.instance.loggedIn = false
        AuthServices.instance.userEmail = ""
        AuthServices.instance.authToken = ""
        MessageServices.instance.clearChannel()
        MessageServices.instance.clearMessages()
        NotificationCenter.default.post(name: NOTIF_USER_CHANEGED, object: nil)
    }
}











