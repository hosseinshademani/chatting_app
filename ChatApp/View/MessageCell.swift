//
//  MessageCellTableViewCell.swift
//  ChatApp
//
//  Created by hossein shademany on 11/29/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    // Outlets
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userTime: UILabel!
    @IBOutlet weak var userMessge: UILabel!
    @IBOutlet weak var userName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(message: Message){
        userImg.image = UIImage(named: message.userAvatar)
        userImg.backgroundColor = UserDataServices.instance.userColor(component: message.userAvatarColor)
        userName.text = message.userName
        userTime.text = message.timeStamp
        userMessge.text = message.messageBody
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    
    
    
    
    
    
}
