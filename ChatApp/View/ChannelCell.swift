//
//  ChannelCell.swift
//  ChatApp
//
//  Created by hossein shademany on 11/19/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet weak var channelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        channelName.textColor = UIColor.white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1779620399)
        }else{
            self.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    func config(channel : Channel){
        let title = channel.channelTitle ?? "default"
        channelName.text = "#\(title)"
     
    }
    
}
