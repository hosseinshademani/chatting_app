//
//  CircleImage.swift
//  ChatApp
//
//  Created by hossein shademany on 11/15/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit
@IBDesignable
class CircleImage: UIImageView {

    override func awakeFromNib() {
          setUp()
    }
    func setUp(){
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
    override func prepareForInterfaceBuilder() {
        setUp()
    }
}
