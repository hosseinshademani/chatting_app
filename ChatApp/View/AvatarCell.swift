//
//  AvatarCell.swift
//  ChatApp
//
//  Created by hossein shademany on 11/13/1396 AP.
//  Copyright © 1396 hossein shademany. All rights reserved.
//

import UIKit
enum AavatarType {
    case dark
    case light
}
class AvatarCell: UICollectionViewCell {

    @IBOutlet weak var cellImage: UIImageView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    func AvatarColor(index : Int , type : AavatarType){
        if type == AavatarType.dark{
            self.cellImage.image = UIImage(named: "dark\(index)")
            self.layer.backgroundColor = UIColor.lightGray.cgColor
        }else{
            self.cellImage.image = UIImage(named: "light\(index)")
            self.layer.backgroundColor = UIColor.gray.cgColor
        }
    }
    func setUpView(){
        self.layer.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }

}
